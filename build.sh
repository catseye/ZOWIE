#!/bin/sh

# SPDX-FileCopyrightText: Chris Pressey, the original author of this work, has dedicated it to the public domain.
# For more information, please refer to <https://unlicense.org/>
# SPDX-License-Identifier: Unlicense

if [ `which rpython`X = X ]; then
    echo 'RPython not found.  Not building.  Use CPython or Skulpt instead.'
else
    python `which rpython` src/zowie.py
    mkdir -p bin
    mv zowie-c bin/
fi
