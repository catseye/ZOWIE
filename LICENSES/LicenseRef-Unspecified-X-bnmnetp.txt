This work is derived from sample code posted at the following URL:

https://gist.github.com/bnmnetp/4650616

The license under which this code has been distributed has not
been specified, however it is believed that it is reasonable
to interpret the manner in which it has been presented as giving
general license to use it as a starting point to build upon to
create derivative works.  It is under this interpretation that
the present work is also distributed.
